#include <iostream>
#include <string>

#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/json.hpp>
#include <bsoncxx/types.hpp>

#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>
#include <mongocxx/stdx.hpp>
#include <mongocxx/uri.hpp>

#include <mongocxx/exception/operation_exception.hpp>


bool isSharded(mongocxx::database& db, const std::string& coll_name) {
  using bsoncxx::builder::basic::kvp;
  using bsoncxx::builder::basic::make_document;

  auto result = db.run_command(make_document(kvp("collStats", coll_name)));
  return result.view()["sharded"].get_bool();
}

int main(int, char**) {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);

  mongocxx::instance instance{};
  mongocxx::uri uri("mongodb://localhost/EOS");
  mongocxx::client client(uri);

  std::string db_name = uri.database();
  if (db_name.empty()) {
    db_name = "EOS";
  }
  mongocxx::database db = client[db_name];

  try {
    std::cout << isSharded(db, "blocks") << '\n';
    std::cout << isSharded(db, "action_traces") << '\n';
    std::cout << isSharded(db, "transaction_traces") << '\n';
  } catch(mongocxx::operation_exception& e) {
    std::cout << "code " << e.code().value() << ", details " << e.code().message() << '\n';
  }

  mongocxx::collection coll = db["action_traces"];

  std::cout << coll.estimated_document_count() << '\n';

  return 0;
}
