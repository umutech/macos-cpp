#include <iostream>

#include <bsoncxx/oid.hpp>

#include <bson/bson.h>

bsoncxx::oid make_custom_oid() {
  bsoncxx::oid x = bsoncxx::oid();
  const char* p = x.bytes();
  std::swap((short&)p[0], (short&)p[10]);
  return x;
}

int main(int, char**) {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);

  std::cout << "OIDs:" << std::endl;
  for (int i = 0; i < 16; ++i) {
    auto oid = bsoncxx::oid().to_string();
    std::cout << '\t' << oid << ", " << bson_oid_is_valid(oid.data(), oid.size()) << std::endl;
  }

  std::cout << "Custom OIDs:" << std::endl;
  for (int i = 0; i < 16; ++i) {
    auto oid = make_custom_oid().to_string();
    std::cout << '\t' << oid << ", " << bson_oid_is_valid(oid.data(), oid.size()) << std::endl;
  }

  return 0;
}
