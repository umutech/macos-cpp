#include <chrono>
#include <future>
#include <iostream>

int main() {
  using std::cout;

  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);

  std::future<int> f1 = std::async(std::launch::async, []() { return 1; });

  cout << f1.get() << '\n'; // output: 1

  std::future<int> f2 =
      std::async(std::launch::async, []() {
        cout << 2 << '\n';
        return 0;
      });

  f2.wait();  // output: 2
  cout << f2.get() << '\n'; // output: 0

  std::future<int> future = std::async(std::launch::async, []() {
    std::this_thread::sleep_for(std::chrono::seconds(3));
    return 3;
  });

  std::cout << "waiting...\n";
  std::future_status status;
  do {
    status = future.wait_for(std::chrono::seconds(1));
    if (status == std::future_status::deferred) {
      std::cout << "deferred\n";
    } else if (status == std::future_status::timeout) {
      std::cout << "timeout\n";
    } else if (status == std::future_status::ready) {
      std::cout << "ready!\n";
    }
  } while (status != std::future_status::ready);

  std::cout << "result is " << future.get() << '\n';

  return 0;
}
