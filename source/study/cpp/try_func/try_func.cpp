#include <iostream>

template<typename T>
void try_func(const T& o) try {
  std::cout << "try" << o << "\n";
} catch (...) {
  std::cout << "catch\n";
}

int main() {
  try_func(618);
  return 0;
}
