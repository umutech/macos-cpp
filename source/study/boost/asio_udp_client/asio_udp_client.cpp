#include <boost/asio.hpp>
//#include <boost/lexical_cast.hpp>
//boost::lexical_cast<uint16_t>(argv[2])
#include <boost/program_options.hpp>

#include <iostream>
#include <random>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>

#ifndef SOCKET_ERROR
#define SOCKET_ERROR -1
#endif

namespace po = boost::program_options;

const size_t kUdpMaxPayloadSize = 1472;

int main(int argc, char *argv[]) {
  std::string address("0.0.0.0");
  uint16_t port = 7474;
  uint16_t payload_size = kUdpMaxPayloadSize;

  po::options_description desc("Allowed options");
  desc.add_options()("address,a", po::value<std::string>(&address), "Input address");
  desc.add_options()("port,p", po::value<uint16_t>(&port), "Input port");
  desc.add_options()("size,s", po::value<uint16_t>(&payload_size), "Input UDP payload size");
  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  boost::asio::io_service io_service;
  boost::asio::ip::udp::socket socket(io_service);
  boost::asio::ip::udp::endpoint end_point(
      boost::asio::ip::address::from_string(address), port);
  socket.open(end_point.protocol());

#if defined(__gnu_linux__) or defined(__APPLE__)
  struct timeval tv = { 1, 0 };
#else
  int tv = 1000;
#endif
  int result = setsockopt(socket.native_handle(), SOL_SOCKET, SO_SNDTIMEO, &tv, sizeof(tv));
  if (SOCKET_ERROR == result) {
    std::cout << "setsockopt(SO_SNDTIMEO) failed with " << errno << '\n';
  }
  result = setsockopt(socket.native_handle(), SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv));
  if (SOCKET_ERROR == result) {
    std::cout << "setsockopt(SO_RCVTIMEO) failed with " << errno << '\n';
  }

#ifdef __gnu_linux__
  socket.connect(end_point);
  std::cout << "connect to " << end_point << '\n';
  uint32_t pmtu = IP_PMTUDISC_DO;
  result = setsockopt(socket.native_handle(), IPPROTO_IP, IP_MTU_DISCOVER, &pmtu, sizeof(pmtu));
  if (SOCKET_ERROR == result) {
    std::cout << "setsockopt() failed with " << errno << '\n';
  }
#endif

  std::string send_buffer;
  send_buffer.resize(payload_size);
  std::string receive_buffer;
  receive_buffer.resize(payload_size);
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<> dis(0, 255);

  for (int send_count = 0; send_count < 10; ++send_count) {
    try {
      for (size_t i = 0; i < send_buffer.size(); ++i) {
        send_buffer.at(i) = dis(gen);
      }
#ifdef __gnu_linux__
      size_t size = socket.send(
          boost::asio::buffer(send_buffer.c_str(), send_buffer.size())
          );

      std::cout << "send " << send_buffer.size() << "bytes.\n";

      size = socket.receive(
          boost::asio::buffer(receive_buffer.data(), receive_buffer.size()), MSG_WAITALL);

      uint32_t mtu = 0;
      socklen_t len = sizeof(mtu);
      int result = getsockopt(socket.native_handle(), IPPROTO_IP, IP_MTU, &mtu, &len);
      if (SOCKET_ERROR == result) {
        std::cout << errno << '\n';
      }
      std::cout << "recv " << size << "bytes, MTU " << mtu << ".\n";
#else
      size_t size = socket.send_to(
          boost::asio::buffer(send_buffer.c_str(), send_buffer.size()), end_point
          );
      std::cout << "send " << send_buffer.size() << " bytes to " << end_point << ".\n";

      size = socket.receive_from(
          boost::asio::buffer(receive_buffer.data(), receive_buffer.size()), end_point, MSG_WAITALL);

      std::cout << "recv " << size << " bytes from " << end_point << ".\n";
#endif
    } catch (boost::system::system_error& e) {
      std::cout << "process failed:" << e.what() << '\n';
    }
  }
}
