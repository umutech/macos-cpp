#include <chrono>
#include <iostream>
#include <thread>

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/lexical_cast.hpp>
//#include <boost/asio/high_resolution_timer.hpp>
#include <boost/asio/steady_timer.hpp>
//#include <boost/asio/system_timer.hpp>

void print(int id) {
  static int count = 0;
  std::cout << boost::lexical_cast<std::string>(std::this_thread::get_id())
    + ": id = " + boost::lexical_cast<std::string>(id) + '\n';
  std::cout << boost::lexical_cast<std::string>(std::this_thread::get_id())
    + ": count = " + boost::lexical_cast<std::string>(++count) + '\n';
}

int main() {
  boost::asio::io_context io;
  boost::asio::io_context::work work(io);
  auto strand(boost::asio::make_strand(io));

  //std::thread t(boost::bind(&boost::asio::io_context::run, &io));
  std::thread tio1([&io]() {
    std::cout << boost::lexical_cast<std::string>(std::this_thread::get_id()) + ": tio1\n";
    io.run();
  });

  std::thread tio2([&io]() {
    std::cout << boost::lexical_cast<std::string>(std::this_thread::get_id()) + ": tio2\n";
    io.run();
  });

  std::thread t1([&]() {
    std::cout << boost::lexical_cast<std::string>(std::this_thread::get_id()) + ": t1\n";
    //io.dispatch(boost::bind(print, 1));
    boost::asio::dispatch(strand, boost::bind(print, 1));
    //io.run();
  });
	std::thread t2([&]() {
    std::cout << boost::lexical_cast<std::string>(std::this_thread::get_id()) + ": t2\n";
    boost::asio::post(strand, []() {
      print(2);
    });
    boost::asio::post(strand, boost::bind(print, 3));
    //io.run();
  });

  std::cout << boost::lexical_cast<std::string>(std::this_thread::get_id()) + ": main\n";
  io.run();

  //t1.join();
  //t2.join();
  //std::cin.get();
  return 0;
}