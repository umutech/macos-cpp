cmake_minimum_required (VERSION 3.5)
project (try_func)

add_executable(try_func try_func.cpp)
set_property(TARGET try_func PROPERTY CXX_STANDARD 17)
