#include <iostream>
#include <random>

#include <bsoncxx/builder/basic/document.hpp>
#include <bsoncxx/builder/basic/kvp.hpp>
#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/json.hpp>
#include <bsoncxx/oid.hpp>

bsoncxx::oid make_custom_oid() {
  bsoncxx::oid x = bsoncxx::oid();
  const char* p = x.bytes();
  std::swap((short&)p[0], (short&)p[10]);
  return x;
}

template<typename T>
void initializer_stream(T& s) {
  bsoncxx::types::b_null null;
  s << "count" << null;
}

int main(int, char**) {
  using bsoncxx::builder::basic::kvp;
  using bsoncxx::builder::basic::make_document;

  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);

  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<> dis(1, 618);

  // make_document 是模板
  std::cout << bsoncxx::to_json(make_document(kvp("value", dis(gen))))
            << std::endl;
  std::cout << bsoncxx::to_json(make_document(kvp("_id", make_custom_oid()),
                                              kvp("value", dis(gen))))
            << std::endl;

  auto doc = bsoncxx::builder::basic::document{};
  std::cout << "empty: " << bsoncxx::to_json(doc) << std::endl;
  doc.append(kvp("value", dis(gen)));
  std::cout << bsoncxx::to_json(doc) << std::endl;
  doc.clear();
  doc.append(kvp("_id", make_custom_oid()));
  doc.append(kvp("value", dis(gen)));
  std::cout << bsoncxx::to_json(doc) << std::endl;

  bsoncxx::builder::stream::document stream_doc{};
  stream_doc << "value" << dis(gen);
  std::cout << bsoncxx::to_json(stream_doc) << std::endl;

  // document 内部元素是有序的
  stream_doc.clear();
  stream_doc << "_id" << make_custom_oid() << "value" << dis(gen);
  std::cout << bsoncxx::to_json(stream_doc) << std::endl;

  bsoncxx::document::value doc_value =
      bsoncxx::builder::stream::document{}
      << "name"
      << "MongoDB"
      << "type"
      << "database"
      << "count" << int64_t(1) << "versions" << bsoncxx::builder::stream::open_array
      << "v4.0"
      << "v3.6"
      << "v3.4" << bsoncxx::builder::stream::close_array << "info"
      << bsoncxx::builder::stream::open_document << "x" << 203 << "y" << 102
      << bsoncxx::builder::stream::close_document
      << bsoncxx::builder::stream::finalize;
  //std::cout << doc_value.view()["count"].to_string() << std::endl;
  std::cout << bsoncxx::to_json(doc_value) << std::endl;

  bsoncxx::builder::stream::document null_doc{};
  initializer_stream(null_doc);
  //bsoncxx::types::b_null null;
  //null_doc << "count" << null;
  bsoncxx::document::value null_dv = null_doc << bsoncxx::builder::stream::finalize;
  std::cout << bsoncxx::to_json(null_dv) << std::endl;

  bsoncxx::document::value array_doc = bsoncxx::from_json("[1, 2, 3, 4]");
  std::cout << bsoncxx::to_json(array_doc) << std::endl;

  bsoncxx::builder::basic::document obj_doc{};
  obj_doc.append(kvp("1", make_document(kvp("2", "value"))));
  std::cout << bsoncxx::to_json(obj_doc) << std::endl;

  bsoncxx::builder::basic::document obj{};
  obj.append(kvp("1", [](bsoncxx::builder::basic::sub_document sub) {
    sub.append(kvp("sub_key", "sub_value"));
  }));
  std::cout << bsoncxx::to_json(obj) << std::endl;

  return 0;
}