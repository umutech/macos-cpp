#include <vector>
#include <iostream>
#include <stdlib.h>


#define X 1
#define _X(x) #x
#define XX(x) "#" _X(x) "#"


int main() {
  std::cout << XX(X) << '\n';

  std::string s("umutech@");
  std::vector<char> vc;

  const char* cc = "UMU Corporation";
  //vc.resize(s.size());
  //vc.assign(s.begin(), s.end());
  vc.assign(cc, cc + 10);
  std::cout << vc.data() << '\n';

  std::vector<char> v;

  v.reserve(s.size());
  for (size_t i = 0; i < s.size(); ++i) {
    v.push_back(s.at(i));
  }
  std::cout << *(v.crbegin() + 0) << '\n';
  std::cout << *(v.crbegin() + 1) << '\n';
  std::cout << *(v.crbegin() + 2) << '\n';

  std::cout << memcmp("tech@", (v.cend() - 5).base(), 5) << '\n';
  std::cout << memcmp("tech@", (v.crbegin() + 5).base().base(), 5) << '\n';

  std::cout << std::equal(v.cend() - 5, v.cend(), "tech@") << '\n';
  std::cout << std::equal(s.cbegin() + 3, s.cend(), v.cend() - 5, v.cend()) << '\n';
  return 0;
}
