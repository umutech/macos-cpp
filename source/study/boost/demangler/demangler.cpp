#include <iostream>

#include <boost/core/demangle.hpp>

int main(int argc, char *argv[]) {
  for (auto i = 1; i < argc; ++i) {
    std::cout << boost::core::demangle(argv[i]) << " => " << argv[i] << '\n';
  }
  return 0;
}
