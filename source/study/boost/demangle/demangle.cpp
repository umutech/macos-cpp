#include <iostream>

#include <boost/core/demangle.hpp>

//#define TYPENAME(X) #X
#define TYPENAME(X) typeid(X).name()

namespace umu {
template <typename Impl>
class plugin {
 public:
  plugin() {
    std::cout << __FUNCTION__ << ": " << TYPENAME(Impl) << " => "
              << boost::core::demangle(TYPENAME(Impl)) << '\n';
  }

  virtual ~plugin() {
    std::cout << __FUNCTION__ << ": " << TYPENAME(bool) << " => "
              << boost::core::demangle(TYPENAME(bool)) << '\n';
  }
};

class test_plugin : public plugin<test_plugin> {
 public:
  virtual ~test_plugin() {
    std::cout << __FUNCTION__ << ": " << TYPENAME(int) << " => "
              << boost::core::demangle(TYPENAME(int)) << '\n';
  }
};
}  // namespace umu

int main() {
  umu::test_plugin test;
  return 0;
}
