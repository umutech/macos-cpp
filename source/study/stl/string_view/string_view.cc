#include <iostream>
#include <string_view>

int main() {
  std::string_view sv("Email: UMUTech@qq.com");
  std::cout << sv << std::endl;  // Email: UMUTech@qq.com
  sv.remove_prefix(7);
  std::cout << sv << std::endl;             // UMUTech@qq.com
  std::cout << sv.data() - 7 << std::endl;  // Email: UMUTech@qq.com

  sv.remove_suffix(4);
  std::cout << sv << std::endl;         // UMUTech@qq
  std::cout << sv.data() << std::endl;  // UMUTech@qq.com

  int n = sv.size();
  char buffer[n];
  strcpy(buffer, sv.data());
  std::cout << "buffer: " <<  buffer << std::endl;

  return 0;
}
