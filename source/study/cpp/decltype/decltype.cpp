#include <iostream>
#include <utility>
#include <tuple>
#include <array>
#include <typeinfo>

struct Test {
  int number{10};
};

void func(Test t);

int main()
{
  int n = Test{}.number;
  std::cout << n << std::endl;
  std::cout << "char: " << typeid(char).name() << std::endl;
  std::cout << "int: " << typeid(int).name() << std::endl;
  std::cout << "long: " << typeid(long).name() << std::endl;
  std::cout << "void: " << typeid(void).name() << std::endl;
  std::cout << typeid(Test).name() << std::endl;
  std::cout << typeid(Test{}).name() << std::endl;
  std::cout << typeid(func(Test{})).name() << std::endl;
  std::cout << typeid(decltype(func(Test{}))).name() << std::endl;
  return 0;
}
