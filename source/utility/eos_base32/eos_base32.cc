//
//  Created by UM U on 2018/9/12.
//  Copyright © 2018年 UM U. All rights reserved.
//

#include <iomanip>
#include <iostream>
#include <string>

#include <boost/program_options.hpp>

namespace po = boost::program_options;


#pragma region "EOSIO"
    // from github.com/EOSIO/eos/contracts/eosiolib/types.hpp
    /**
    *  Converts a base32 symbol into its binary representation, used by string_to_name()
    *
    *  @brief Converts a base32 symbol into its binary representation, used by string_to_name()
    *  @param c - Character to be converted
    *  @return constexpr char - Converted character
    *  @ingroup types
    */
    static constexpr  char char_to_symbol( char c ) {
        if( c >= 'a' && c <= 'z' )
            return (c - 'a') + 6;
        if( c >= '1' && c <= '5' )
            return (c - '1') + 1;
        return 0;
    }

    /**
    *  Converts a base32 string to a uint64_t. This is a constexpr so that
    *  this method can be used in template arguments as well.
    *
    *  @brief Converts a base32 string to a uint64_t.
    *  @param str - String representation of the name
    *  @return constexpr uint64_t - 64-bit unsigned integer representation of the name
    *  @ingroup types
    */
    static constexpr uint64_t string_to_name( const char* str ) {
        uint32_t len = 0;
        while( str[len] ) ++len;

        uint64_t value = 0;

        for( uint32_t i = 0; i <= 12; ++i ) {
            uint64_t c = 0;
            if( i < len && i <= 12 ) c = uint64_t(char_to_symbol( str[i] ));

            if( i < 12 ) {
            c &= 0x1f;
            c <<= 64-5*(i+1);
            }
            else {
            c &= 0x0f;
            }

            value |= c;
        }

        return value;
    }

    static void trim_right_dots(std::string& str ) {
        const auto last = str.find_last_not_of('.');
        if (last != std::string::npos)
            str = str.substr(0, last + 1);
    }

    // Modified by UMU
    std::string to_string(uint64_t value) {
        static const char* charmap = ".12345abcdefghijklmnopqrstuvwxyz";
        
        std::string str(13,'.');
        
        uint64_t tmp = value;
        for( uint32_t i = 0; i <= 12; ++i ) {
            char c = charmap[tmp & (i == 0 ? 0x0f : 0x1f)];
            str[12-i] = c;
            tmp >>= (i == 0 ? 4 : 5);
        }
        
        trim_right_dots( str );
        return str;
    }

    // from github.com/EOSIO/eos/contracts/eosiolib/symbol.hpp
    /**
    * Converts string to uint64_t representation of symbol
    *
    * @param precision - precision of symbol
    * @param str - the string representation of the symbol
    */
    static constexpr uint64_t string_to_symbol( uint8_t precision, const char* str ) {
        uint32_t len = 0;
        while( str[len] ) ++len;

        uint64_t result = 0;
        for( uint32_t i = 0; i < len; ++i ) {
            if( str[i] < 'A' || str[i] > 'Z' ) {
            /// ERRORS?
            } else {
            result |= (uint64_t(str[i]) << (8*(1+i)));
            }
        }

        result |= uint64_t(precision);
        return result;
    }

    /**
    * uint64_t representation of a symbol name
    */
    typedef uint64_t symbol_name;

    /**
    * Checks if provided symbol name is valid.
    *
    * @param sym - symbol name of type symbol_name
    * @return true - if symbol is valid
    */
    static constexpr bool is_valid_symbol( symbol_name sym ) {
        sym >>= 8;
        for( int i = 0; i < 7; ++i ) {
            char c = (char)(sym & 0xff);
            if( !('A' <= c && c <= 'Z')  ) return false;
            sym >>= 8;
            if( !(sym & 0xff) ) {
            do {
                sym >>= 8;
                if( (sym & 0xff) ) return false;
                ++i;
            } while( i < 7 );
            }
        }
        return true;
    }

    /**
    * Returns the character length of the provided symbol
    *
    * @param sym - symbol to retrieve length for (uint64_t)
    * @return length - character length of the provided symbol
    */
    static constexpr uint32_t symbol_name_length( symbol_name sym ) {
        sym >>= 8; /// skip precision
        uint32_t length = 0;
        while( sym & 0xff && length <= 7) {
            ++length;
            sym >>= 8;
        }

        return length;
    }
#pragma endregion

uint64_t string_to_uint64(const std::string& str) {
    std::istringstream is(str);
    uint64_t result;
    int base = 10;
    if (str.compare(0, 2, "0x") == 0) {
        base = 16;
    }
    is >> std::setbase(base) >> result;
    return result;
}

int main(int argc, const char * argv[]) {
    try {
        po::options_description desc("Allowed options");
        desc.add_options()
            ("help,h", "produce help message")
            ("decode,d", po::value<std::string>(), "Decode BASE32 string to uint64_t")
            ("encode,e", po::value<std::string>(), "Encode uint64_t to BASE32 string")
            ("string_to_symbol,S", "Converts string to uint64_t representation of symbol")
            ("precision,p", po::value<int>(), "Precision of symbol")
            ("string,s", po::value<std::string>(), "The string representation of the symbol")
            ("is_valid_symbol,i", po::value<std::string>(), "Checks if provided symbol name is valid.")
            ("symbol_name_length,l", po::value<std::string>(), "Returns the character length of the provided symbol")
        ;

        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);    

        if (vm.count("help")) {
            std::cout << desc << "\n";
            return 0;
        } else if (vm.count("decode")) {
            auto arg = vm["decode"].as<std::string>();
            auto i = string_to_name(arg.data());
            std::cout << "Decode(\"" << arg << "\") to " << std::dec << i
                << " (0x" << std::hex << std::setw(16) << std::setfill('0') << i << ")\n";
            return 0;
        } else if (vm.count("encode")) {
            auto arg = vm["encode"].as<std::string>();
            auto i = string_to_uint64(arg);
            std::cout << "Encode(" << arg << ") to " << to_string(i) << "\n";
            return 0;
        } else if (vm.count("string_to_symbol")) {
            if (0 == vm.count("precision")) {
                std::cout << "precision MUST be provided.\n";
                return 1;
            }
            if (0 == vm.count("string")) {
                std::cout << "string MUST be provided.\n";
                return 1;
            }

            auto p = vm["precision"].as<int>();
            auto s = vm["string"].as<std::string>();
            uint64_t symbol = string_to_symbol(p, s.data());
            std::cout << "S(" << p << ", " << s << ") = " << symbol <<
                " (0x" << std::hex << std::setw(16) << std::setfill('0') << symbol << ")\n";
            return 0;
        } else if (vm.count("is_valid_symbol")) {
            auto arg = vm["is_valid_symbol"].as<std::string>();
            auto symbol = string_to_uint64(arg);
            std::cout << "is_valid_symbol(" << symbol << ") = " << std::boolalpha << is_valid_symbol(symbol) << "\n";
        } else if (vm.count("symbol_name_length")) {
            auto arg = vm["symbol_name_length"].as<std::string>();
            auto symbol = string_to_uint64(arg);
            std::cout << "symbol_name_length(" << symbol << ") = " << std::boolalpha << symbol_name_length(symbol) << "\n";
        }
    } catch(std::exception& e) {
        std::cout << e.what() << "\n";
        return -1;
    }
    return 1;
}
