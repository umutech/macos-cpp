#include <iostream>
#include <chrono>
#include <ctime>
#include <cmath>

bool is_prime(uint64_t n) {
  if (n < 2) {
    throw;
  }
  if (n == 2 || n == 3) {
    return true;
  }
  if ((n & 1) == 0) {
    return false;
  }
  if (n % 3 == 0) {
    return false;
  }

  for (uint64_t i = 5; i <= std::sqrt(n); i += 6) {
    if (n % i == 0) {
      return false;
    }
    if (n % (i + 2) == 0) {
      return false;
    }
  }
  return true;
}

uint64_t try_test_0(uint64_t n) {
  if (n < 1000) {
    return n * 2 + 1;
  }
  try {
    if (is_prime(n)) {
      n = n * n + 1;
    } else {
      n += 3;
    }
  } catch (std::exception e) {
    std::cout << e.what() << '\n';
  }
  return n;
}

uint64_t try_test_1(uint64_t n) {
  try {
    if (n < 1000) {
      return n * 2 + 1;
    }
    if (is_prime(n)) {
      n = n * n + 1;
    } else {
      n += 3;
    }
  } catch (std::exception e) {
    std::cout << e.what() << '\n';
  }
  return n;
}

int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);

  using namespace std::chrono;

  {
    auto now = system_clock::now();
    uint64_t total = 0;
    for (uint64_t i = 0; i < 40000000; ++i) {
      total += try_test_1(i * 2 + 618);
    }
    auto diff = system_clock::now() - now;
    std::cout << "try: " << diff.count() << ' ' << total << '\n';
  }

  {
    auto now = system_clock::now();
    uint64_t total = 0;
    for (uint64_t i = 0; i < 40000000; ++i) {
      total += try_test_0(i * 2 + 618);
    }
    auto diff = system_clock::now() - now;
    std::cout << "No try: " << diff.count() << ' ' << total << '\n';
  }

  return 0;
}
