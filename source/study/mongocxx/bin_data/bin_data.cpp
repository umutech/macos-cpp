#include <iostream>

#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/json.hpp>
#include <bsoncxx/types.hpp>

int main(int, char**) {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);

  bsoncxx::types::b_binary bin;
  bin.sub_type = bsoncxx::binary_sub_type::k_binary;
  bin.size = 32;
  bin.bytes = reinterpret_cast<const uint8_t *>("\x01\x23\x45\x67\x89\x12\x34\x56\x78\x90\x01\x23\x45\x67\x89\x12\x34\x56\x78\x90\x01\x23\x45\x67\x89\x12\x34\x56\x78\x90\xAB\xCD");
  bsoncxx::builder::stream::document stream_doc{};
  stream_doc << "hex_value" << bin;
  std::cout << bsoncxx::to_json(stream_doc) << std::endl;

  return 0;
}
