#include <iostream>
#include <sstream>

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

namespace po = boost::program_options;
namespace fs = boost::filesystem;

int main(int argc, char* argv[]) {
  int result = 0;

  try {
    fs::path inPath;

    po::options_description desc("Allowed options");
    // to fix: std::string path; po::value<std::string>(&path); inPath = path;
    desc.add_options()("in,i", po::value<fs::path>(&inPath), "Input file or directory <pathname>");

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (0 == vm.count("in")) {
      throw std::invalid_argument("What? No input path specifed!");
    }

    std::cout << "Input path = \"" << inPath.string() << "\"\n";
  } catch (const fs::filesystem_error& e) {
    std::cerr << "\nFilessytem error: " << e.what() << std::endl;
    result = 3;
  } catch (const std::exception& e) {
    std::cerr << "\nException error: " << e.what() << std::endl;
    result = 2;
  } catch (...) {
    std::cerr << "\nError: Caught an unknown exception!" << std::endl;
    result = 1;
  }

  return result;
}