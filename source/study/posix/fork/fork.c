#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char* argv[]) {
  pid_t pid = fork();
  if (pid < 0) {
    fprintf(stderr, "错误！\n");
  } else if (pid == 0) {
    printf("子进程空间, argc = %d\n", argc);
  } else {
    printf("父进程空间, 子进程 pid 为 %d，argc = %d\n", pid, argc);
  }
  for (int i = 1; i < argc; ++i) {
    printf("  argv[%d] = %s\n", i, argv[i]);
  }
  return 0;
}