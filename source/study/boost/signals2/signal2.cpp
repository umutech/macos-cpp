#include "boost/utility/result_of.hpp"
#include "boost/typeof/typeof.hpp"
#include "boost/assign.hpp"
#include "boost/ref.hpp"
#include "boost/bind.hpp"
#include "boost/function.hpp"
#include "boost/signals2.hpp"
#include "iostream"

using namespace std;
 
 
template<int N>
struct Slot {
	int operator()(int x) {
		cout << "Slot current N * x value is : " << N * x << endl;
		return (N * x);
	}
};
 
int main(int argc, char* argv[]) {
	boost::signals2::signal<int(int)> sig;
 
	sig.connect(Slot<10>());
	sig.connect(Slot<100>());
	cout << *sig(2) << endl;;
 
	return 0;
}
