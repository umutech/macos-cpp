#pragma once

#include <stdint.h>
#include <algorithm>
#include <string>
#include <vector>

namespace umu {
namespace string {

#pragma region "Split"
#if UNICODE
#define tstring std::wstring
#else
#define tstring std::string
#endif
#pragma endregion

#pragma region "Split"
// source_string 末尾如果是 separator，其后的空白不被加入
// container，只有中间的空白会被加入 container
template <class StringType, class StringOrCharType>
inline size_t split(std::vector<StringType>* container,
                    const StringType& source_string,
                    const StringOrCharType separator,
                    const typename StringType::size_type separator_size) {
  if (container != nullptr) {
    container->clear();
  }
  const typename StringType::size_type source_size = source_string.length();
  size_t size = 0;
  typename StringType::size_type start = 0;
  typename StringType::size_type end;

  do {
    end = source_string.find(separator, start);
    StringType e(source_string.substr(start, end - start));
    if (nullptr != container) {
      container->push_back(e);
    }
    start = end + separator_size;
    ++size;
  } while (start < source_size && StringType::npos != end);

  return size;
}

template <class StringType>
inline size_t Split(std::vector<StringType>* container,
                    const StringType& source_string,
                    const StringType& separator) {
  const typename StringType::size_type separator_size =
      static_cast<StringType>(separator).length();
  return split(container, source_string, separator, separator_size);
}

template <class StringType>
inline size_t Split(std::vector<StringType>* container,
                    const StringType& source_string,
                    const typename StringType::value_type separator) {
  const typename StringType::size_type separator_size = 1;
  return split(container, source_string, separator, separator_size);
}

template <class StringType, typename CharType>
inline typename StringType::size_type Split(std::vector<StringType>* container,
                                            const StringType& source_string,
                                            const CharType* separator) {
  return Split(container, source_string, StringType(separator));
}

template <class StringType, typename CharType>
inline typename StringType::size_type Split(std::vector<StringType>* container,
                                            const CharType* source_string,
                                            const CharType* separator) {
  return Split(container, StringType(source_string), StringType(separator));
}
#pragma endregion

}  // end of namespace string
}  // end of namespace umu
