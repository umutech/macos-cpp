#include <iostream>
#include <chrono>
#include <ctime>

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    using namespace std::chrono;

    auto now = time_point_cast<seconds>(system_clock::now());
    // UNIX timestamp means: seconds since epoch
    std::cout << "UNIX timestamp: " << now.time_since_epoch().count() << std::endl;

    return 0;
}
