cmake_minimum_required (VERSION 3.5)
project (2_threads)

add_executable(2_threads 2_threads.cpp)
set_property(TARGET 2_threads PROPERTY CXX_STANDARD 17)
