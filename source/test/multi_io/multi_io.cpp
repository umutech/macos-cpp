#include <boost/asio.hpp>
#include <chrono>
#include <iostream>
#include <thread>
//#include <boost/asio/high_resolution_timer.hpp>
#include <boost/asio/steady_timer.hpp>
//#include <boost/asio/system_timer.hpp>

class printer {
 private:
  printer() : timer_(io_), count_(0) {}
  ~printer() {}

  void print() {
    if (count_ < 10) {
      std::cout << count_ << "\n";
      ++count_;

      // CPU 400%
      //for (;;);

      timer_.expires_from_now(std::chrono::milliseconds(50));
      timer_.async_wait(std::bind(&printer::print, this));
    } else {
      std::cout << "Final count is " << count_ << "\n";
      delete this;
    }
  }
  void run() {
    timer_.expires_from_now(std::chrono::milliseconds(50));
    timer_.async_wait(std::bind(&printer::print, this));
    io_.run();
  }

 public:
  static printer* Create() { return new printer; }

  void start() {
    std::thread t;
    t = std::thread(std::mem_fn(&printer::run), this);
    t.detach();
  }

 private:
  private:
  boost::asio::io_service io_;
  boost::asio::steady_timer timer_;
  int count_;
};

void foo() {
  printer* p = printer::Create();
  p->start();
}

int main() {
  foo();
  foo();
  foo();
  foo();
  std::cin.get();
  return 0;
}