#include <iostream>
#include <memory>

#include <boost/preprocessor/seq/for_each.hpp>

namespace umu {
template <typename Impl>
class plugin {
 public:
  plugin() {
    std::cout << __FUNCTION__ << '\n';
  }

  virtual ~plugin() {
    std::cout << __FUNCTION__ << '\n';
  }
};

class depended_plugin : public plugin<depended_plugin> {
 public:
  depended_plugin() {
    std::cout << __FUNCTION__ << '\n';
  }

  virtual ~depended_plugin() {
    std::cout << __FUNCTION__ << '\n';
  }
};

class test_plugin : public plugin<test_plugin> {
 public:
  test_plugin() {
    std::cout << __FUNCTION__ << '\n';
  }

  virtual ~test_plugin() {
    std::cout << __FUNCTION__ << '\n';
  }
};

#define MACRO( r, visitor, elem ) \
  std::cout << __FUNCTION__ << ": " << r << ", " << #elem << '\n'; \
  auto p##r = std::make_unique<elem>();

#define REQUIRES( PLUGINS ) \
  BOOST_PP_SEQ_FOR_EACH(MACRO, _, PLUGINS)

void work() {
  //depended_plugin p1;
  //test_plugin p2;
  REQUIRES((depended_plugin)(test_plugin));
}
}  // namespace umu

int main() {
  umu::work();
  return 0;
}
