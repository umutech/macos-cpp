# EOS Base32 转换

UMU @ 11:31 2018/09/27

## 用例

```
$ ./eos_base32 -d shengxiaokai
Decode("shengxiaokai") to 14075216089888066784 (0xc3553675c6a40ce0)

$ ./eos_base32 -e 14075216089888066784
Encode(14075216089888066784) to shengxiaokai

$ ./eos_base32 -e 0xc3553675c6a40ce0
Encode(0xc3553675c6a40ce0) to shengxiaokai

$ ./eos_base32 -S -p 4 -s REX
S(4, REX) = 1480937988 (0x0000000058455204)

$ ./eos_base32 -i 0x0000000058455204
is_valid_symbol(1480937988) = true

$ ./eos_base32 -l 0x0000000058455204
symbol_name_length(1480937988) = 3
```
