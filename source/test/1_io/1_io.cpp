#include <chrono>
#include <iostream>
#include <thread>

#include <boost/asio.hpp>
//#include <boost/asio/high_resolution_timer.hpp>
#include <boost/asio/steady_timer.hpp>
//#include <boost/asio/system_timer.hpp>

class printer {
 private:
  printer(boost::asio::io_service& io)
      : timer_(io, delay_), count_(0) {
    std::cout << "Start\n";
    timer_.async_wait(std::bind(&printer::print, this));
  }

  ~printer() {}

  void print() {
    if (count_ < 10) {
      std::cout << count_ << "\n";
      ++count_;

      // CPU 100%
      //for (;;);

      timer_.expires_from_now(delay_);
      timer_.async_wait(std::bind(&printer::print, this));
    } else {
      std::cout << "Final count is " << count_ << "\n";
      delete this;
    }
  }

 public:
  static printer* Create(boost::asio::io_service& io) {
    return new printer(io);
  }

 private:
  boost::asio::steady_timer timer_;
  int count_;
  static constexpr auto delay_ = std::chrono::milliseconds(500);
};

int main() {
  boost::asio::io_service io;
  printer::Create(io);
  printer::Create(io);
  printer::Create(io);
  printer::Create(io);
  io.run();
  //std::cin.get();
  return 0;
}