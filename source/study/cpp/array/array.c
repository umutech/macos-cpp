#include <stdio.h>

int main(int argc, char const *argv[]) {
  static const char *const abi_tag_os[] = {
    [0] = "Linux",
    [1] = "Hurd",
    [2] = "Solaris",
    [3] = "FreeBSD",
    [4] = "kNetBSD",
    [5] = "Syllable",
    [6] = "Unknown OS"
  };
  printf("__STDC_VERSION__ = %ld\n", __STDC_VERSION__);
  printf("[%d] = %s\n", argc, abi_tag_os[argc]);
}
