#include <iostream>

int main(int argc, const char * argv[]) {
  std::string buffer(argv[1]);
  char *end = nullptr;
  uint64_t u = strtoull(buffer.data(), &end, 10);

  std::cout << u << '\n';
  std::cout << (buffer.data() + buffer.size() == end) << '\n';
  return 0;
}
