#include <boost/asio.hpp>
#include <boost/program_options.hpp>

#include <iostream>
#include <string>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>

#define SOCKET_ERROR -1

namespace po = boost::program_options;

const size_t kUdpMaxPayloadSize = 1472;

int main(int argc, char *argv[]) {
  std::string address("0.0.0.0");
  uint16_t port = 7474;

  po::options_description desc("Allowed options");
  desc.add_options()("address,a", po::value<std::string>(&address), "Input address");
  desc.add_options()("port,p", po::value<uint16_t>(&port), "Input port");
  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  boost::asio::io_service io_service;
  boost::asio::ip::udp::socket udp_socket(io_service);
  boost::asio::ip::udp::endpoint local_add(
      boost::asio::ip::address::from_string(address), port);

  udp_socket.open(local_add.protocol());
  udp_socket.bind(local_add);

  std::cout << "listening on " << address << ":" << port << '\n';

#ifdef __gnu_linux__
  uint32_t pmtu = IP_PMTUDISC_DO;
  setsockopt(udp_socket.native_handle(), IPPROTO_IP, IP_MTU_DISCOVER, &pmtu, sizeof(pmtu));
#endif

  char receive_buffer[kUdpMaxPayloadSize] = {};
  while (true) {
    boost::asio::ip::udp::endpoint send_point;
    size_t size = udp_socket.receive_from(
        boost::asio::buffer(receive_buffer, sizeof(receive_buffer)),
        send_point);
#ifdef __gnu_linux__
    uint32_t mtu = 0;
    socklen_t len = sizeof(mtu);
    int result = getsockopt(udp_socket.native_handle(), IPPROTO_IP, IP_MTU, &mtu, &len);
    if (SOCKET_ERROR == result) {
      std::cout << errno << '\n';
    }
    std::cout << "recv " << size << " bytes from " << send_point << ", MTU " << mtu << ".\n";
#else
    std::cout << "recv " << size << " bytes from " << send_point << ".\n";
#endif
    udp_socket.send_to(boost::asio::buffer(receive_buffer, size), send_point);
  }

  return 1;
}
