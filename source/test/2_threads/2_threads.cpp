#include <iostream>
#include <thread>

int main() {
  std::cout << "Starting thread...\n";
  std::thread t([]() {
    std::cout << "Starting dead loop in new thread...\n";
    for (;;) {
    }
  });
  std::cout << "Starting dead loop...\n";
  for (;;) {
  }
  t.join();
  return 0;
}
